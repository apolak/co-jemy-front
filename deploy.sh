#!/usr/bin/env bash

if [ ! -d "co-jemy-front" ]; then mkdir co-jemy-front; fi
cd co-jemy-front
if [ ! -f "shippable.yml" ]; then
    git clone git@bitbucket.org:apolak/co-jemy-front.git .
fi
if [ -f "shippable.yml" ]; then
    git checkout master
    git pull origin master

    docker-compose -f docker/prod.yml down
    docker-compose -f docker/prod.yml build --no-cache --pull web
    docker-compose -f docker/prod.yml up -d web
fi