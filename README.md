# Co-jemy Frontend app


## Requirements

- Docker
- VirtualBox

## Installation:

```
$ docker-machine create co-jemy --driver virtualbox
```

```
$ eval "$(docker-machine env co-jemy)"
```

```
$ docker-machine ip co-jemy
```

Developement project setup with webpack dev server:

```
$ docker-compose -f docker/dev.yml up -d --build
```

To see webpack logs use:

```
$ docker-compose -f docker/dev.yml logs -f
```

You can check app on:

```
http://docker-machine-ip/
```

## Building project

To build production-ready package, just run:

```
$ docker-compose -f docker/prod.yml up -d --build --force-recreate
```

## Testing

#### 1. Unit Tests

* single run: `docker-compose -f docker/dev.yml run web npm test`

#### 2. End-to-End Tests

* single run:
  * in a tab, *if not already running!*: `npm start`
  * in a new tab: `npm run webdriver-start`
  * in another new tab: `npm run e2e`
* interactive mode:
  * instead of the last command above, you can run: `npm run e2e-live`
  * when debugging or first writing test suites, you may find it helpful to try out Protractor commands without starting up the entire test suite. You can do this with the element explorer.
  * you can learn more about [Protractor Interactive Mode here](https://github.com/angular/protractor/blob/master/docs/debugging.md#testing-out-protractor-interactively)

## Documentation

You can generate api docs (using [TypeDoc](http://typedoc.io/)) for your code with the following:

    npm run docs

Have fun!
