'use strict';

let express = require('express'),
    path = require('path'),
    fs = require('fs');

let app = express();
let staticRoot =  __dirname + '/../dist/';

app.set('port', 80);

app.use(express.static(staticRoot));

app.use((req, res, next) => {

    // if the request is not html then move along
    let accept = req.accepts('html', 'json', 'xml');
    if(accept !== 'html'){
        return next();
    }

    // if the request has a '.' assume that it's for a file, move along
    let ext = path.extname(req.path);
    if (ext !== ''){
        return next();
    }

    fs.createReadStream(staticRoot + 'index.html').pipe(res);

});

app.listen(app.get('port'), () => {
    console.log('app running on port', app.get('port'));
});