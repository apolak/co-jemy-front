export class Supplier {

  constructor(public id: number,
              public name: string,
              public deliveryPrice: number,
              public freeDeliveryPrice: number) {

  }

}
